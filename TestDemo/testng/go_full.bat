set JAVAC5_HOME=javac
set JAVA5_HOME=java

set CPATH=.;../WebContent/WEB-INF/lib/activation-1.1.jar;^
../WebContent/WEB-INF/lib/bsh-core-2.0b4.jar;^
../WebContent/WEB-INF/lib/c3p0-0.9.1.jar;^
../WebContent/WEB-INF/lib/cglib-2.1_3-src.jar;^
../WebContent/WEB-INF/lib/cglib-nodep-2.1_3.jar;^
../WebContent/WEB-INF/lib/commons-codec-1.4.jar;^
../WebContent/WEB-INF/lib/commons-lang-2.0.jar;^
../WebContent/WEB-INF/lib/commons-pool.jar;^
../WebContent/WEB-INF/lib/hamcrest-core-1.1.jar;^
../WebContent/WEB-INF/lib/hamcrest-library-1.1.jar;^
../WebContent/WEB-INF/lib/httpunit.jar;^
../WebContent/WEB-INF/lib/jmock-2.5.1.jar;^
../WebContent/WEB-INF/lib/jmock-junit3-2.5.1.jar;^
../WebContent/WEB-INF/lib/jmock-junit4-2.5.1.jar;^
../WebContent/WEB-INF/lib/jmock-legacy-2.5.1.jar;^
../WebContent/WEB-INF/lib/jmock-script-2.5.1.jar;^
../WebContent/WEB-INF/lib/js-1.6R5.jar;^
../WebContent/WEB-INF/lib/jtidy-4aug2000r7-dev.jar;^
../WebContent/WEB-INF/lib/junit-4.8.2.jar;^
../WebContent/WEB-INF/lib/junit3.jar;^
../WebContent/WEB-INF/lib/junitee-anttask.jar;^
../WebContent/WEB-INF/lib/junitee.jar;^
../WebContent/WEB-INF/lib/mail-1.4.jar;^
../WebContent/WEB-INF/lib/mchange-commons-java-0.2.7.jar;^
../WebContent/WEB-INF/lib/mysql-connector-java-5.1.7-bin.jar;^
../WebContent/WEB-INF/lib/nekohtml-0.9.5.jar;^
../WebContent/WEB-INF/lib/objenesis-1.0.jar;^
../WebContent/WEB-INF/lib/servlet-api-2.4.jar;^
../WebContent/WEB-INF/lib/testng-2.0beta-jdk15.jar;^
../WebContent/WEB-INF/lib/testng.jar;^
../WebContent/WEB-INF/lib/xercesImpl-2.6.1.jar;^
../WebContent/WEB-INF/lib/xmlParserAPIs-2.6.1.jar;../WebContent/WEB-INF/classes;../src/;../test

REM %JAVAC5_HOME% -classpath %CPATH% *.java
%JAVA5_HOME% -ea -classpath %CPATH% org.testng.TestNG testngnew.xml
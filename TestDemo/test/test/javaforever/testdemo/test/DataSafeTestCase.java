package test.javaforever.testdemo.test;

import java.sql.Connection;

import junit.framework.TestCase;

import com.javaforever.testdemo.database.DBConf;


/**
 * DataSafeTestCase must owned by Project Manager
 * Do not change it while you are a developer.
 * 
 * @author Jerry Shen
 * @email jerry_shen_sjf@qq.com
 *
 */
public class DataSafeTestCase extends TestCase{
	
	public DataSafeTestCase(){
		super();
		if (DBConf.isTestsuiteOffline() == true){ 
			Thread.currentThread().destroy();
		}
	}
	
	final protected  void setUp() throws Exception {
		super.setUp();
		//Thread.sleep(500);
		DBConf.switchToTest();		
		System.out.println("Jerry::DataSafeTestCase::setup()");
	}
	
	final protected void tearDown() throws Exception {
		super.tearDown();
		DBConf.switchToProduction();
		System.out.println("Jerry::DataSafeTestCase::teardown()");
	}

}

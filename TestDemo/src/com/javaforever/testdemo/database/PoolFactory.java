package com.javaforever.testdemo.database;

import org.apache.commons.pool.PoolableObjectFactory;   

public class PoolFactory implements PoolableObjectFactory {   

 private static int counter = 0;   

  
 public Object makeObject() throws Exception {   
     Object obj = String.valueOf(counter++);   
     System.err.println("Making Object " + obj);   
     return obj;   
 }   

 public void activateObject(Object obj) throws Exception {   
     System.err.println("Activating Object " + obj);   
 }   

  
 public void passivateObject(Object obj) throws Exception {   
     System.err.println("Passivating Object " + obj);   
 }   

  public boolean validateObject(Object obj) {   
       
     boolean result = (Math.random() > 0.5);   
     System.err.println("Validating Object "  
             + obj + " : " + result);   
     return result;   
 }   

 public void destroyObject(Object obj) throws Exception {   
     System.err.println("Destroying Object " + obj);   
 }   

}
package com.javaforever.testdemo.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;

public final class ConnectionManager {
	static ConnectionManager instance;

	public static ComboPooledDataSource ds;
	private static String c3p0Properties = "c3p0.properties";
	private static String c3p0TestProperties = "c3p0.properties";

	private ConnectionManager(boolean isTest) throws Exception {
		Properties p = new Properties();
		if (isTest)
			p.load(this.getClass().getResourceAsStream(c3p0TestProperties));
		else
			p.load(this.getClass().getResourceAsStream(c3p0Properties));
		ds = new ComboPooledDataSource();
	}

	public static final ConnectionManager getInstance(boolean isTest) {
		if (instance == null) {
			try {
				instance = new ConnectionManager(isTest);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return instance;
	}

	public synchronized final Connection getConnection() {
		try {
			return ds.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected void finalize() throws Throwable {
		DataSources.destroy(ds); // 关闭datasource
		super.finalize();
	}

}
package com.javaforever.testdemo.database;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * DBConf must owned by the Project Manager
 * You can change the configure to fit your situation.
 * 
 * This class should not change others except the databasename,
 * username and password
 * 
 * @author Jerry Shen
 * @release under GPLv2
 * @email jerry.shen.sjf@gmail.com
 *
 */
public final class DBConf implements AutoCloseable{    
    protected static boolean isNotTest = false;					// Do not change this value, change them in dbconfig.xml	
    protected static boolean isProductProtectLockOffline = false;  	// In production: set true ; In test: set false
    protected static boolean isTestsuiteOffline = false;			// Test suite offline: set true ; Test suite online: set false
    protected static boolean isGpinterfaceOffline = false;	    // Testing gpinterface offline: set true ; Test suite online: set false
    protected static DataSource ds;
    protected static Connection connection;
    protected static String testDatabaseURL;
    protected static String testDatabaseUName;
    protected static String testDatabasePWord;
    protected static String testDatabaseName;
    
    static {
    	ReadConfigXml reader = new ReadConfigXml("dbconfig.xml"); 
   	
    	isNotTest = reader.isNotTest();
    	isTestsuiteOffline = reader.isTestsuiteOffline();
    	isProductProtectLockOffline = reader.isProductProtectLockOffline();
    	isGpinterfaceOffline = reader.isGpinterfaceOffline();
    	
    	testDatabaseURL = reader.getTestUrl();
    	testDatabaseName = reader.getTestDataBase();
    	testDatabaseUName  = reader.getTestUserName();
    	testDatabasePWord = reader.getTestPassWord();
    	
		ds = new ComboPooledDataSource();
    }

	public static Connection initDB() throws Exception{
		if (isNotTest && isProductProtectLockOffline) return ds.getConnection();
		else {	
			Class.forName("com.mysql.jdbc.Driver");
            String url = testDatabaseURL + testDatabaseName + "?useUnicode=true&characterEncoding=utf-8";
            System.out.println("JerryDebugMysql:"+ url);
            connection = DriverManager.getConnection(url,testDatabaseUName,testDatabasePWord);
            return connection;
        }
    }
	
	public static synchronized Connection initTestDB() throws Exception{
		isNotTest = false;
		return initDB();
    }
	
	public static Connection initProductionDB() throws Exception{
		return ds.getConnection();
    }
	
    public static synchronized void closeDB(Connection connection){
        try {
        	connection.close();
        	connection = null;
        } catch (Throwable tb){
       }
   }
    
     public static void switchToTest(){
    	isNotTest = false;
     }
     
     public  static void switchToProduction(){
    	 isNotTest = true;
    }

	public static boolean isTestsuiteOffline() {
		return isTestsuiteOffline;
	}

	public static boolean isGpinterfaceOffline() {
		return isGpinterfaceOffline;
	}

	@Override
	public void close() throws Exception {
		connection.close();
	}	
}

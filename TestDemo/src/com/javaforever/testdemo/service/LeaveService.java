package com.javaforever.testdemo.service;

import java.util.List;

import com.javaforever.testdemo.domain.Leave;

public interface LeaveService {
	public boolean createLeave(Leave leave) throws Exception;
	public boolean updateLeave(Leave leave) throws Exception;
	public boolean deleteLeave(long id) throws Exception;
	public List<Leave> listAllLeaves() throws Exception; 
	public Leave findLeaveById(long id) throws Exception;
	public List<Leave> getLeaveListByEmpid(long empid) throws Exception;
}
